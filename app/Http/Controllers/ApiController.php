<?php

namespace App\Http\Controllers;

use App\Product;
use App\ProductCategory;
use App\Http\Requests;
use Response;

class ApiController extends Controller
{
    public function getProductDetails($id)
    {
        //Раньше я REST API не делал
        $product = Product::find($id);
        $product->categiries = ProductCategory::where('product_id',$id)
           ->join('categories', 'category_id', '=','categories.id')
           ->select('categories.id', 'title')
           ->get();
        $product->images = $product->images;
        return Response::json($product);
    }
}

<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;
use App\Http\Requests;
use Laracasts\Flash\Flash;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        $menu = '<ul>';
        $categories_all = Category::orderBy('parent_id')
            ->get();
        foreach ($categories_all as $categories_one) {
            $categories[$categories_one->id] = $categories_one;
        }
        $tree = '';
//        Постройка дочерних элементов
        if (isset($categories)){
            foreach ($categories as $item) {
                if ($item["parent_id"]) {
                    $childrens[$item["id"]] = $item["parent_id"];
                }
            }
//            Вывод дочерних элементов
            function qq($item, $categories, $childrens, $menu)
            {
                $menu .= '<li><a href="/' . $item->id . '">' . $item->title . '</a></li>';
                $ul = false;
                while (true) {
                    $key = array_search($item->id, $childrens);
                    if (!$key) {
                        if ($ul) $menu .= "</ul>";
                        break;
                    }
                    unset($childrens[$key]);
                    if (!$ul) {
                        $menu .= "<ul>";
                        $ul = true;
                    }
                    $menu = qq($categories[$key], $categories, $childrens, $menu);
                }
                return $menu;
            }
//            Вывод главных элементов
            foreach ($categories as $category) {
                if ($category->parent_id == '0') {
                    $menu = qq($category, $categories, $childrens, $menu);
                }
            }
        }
        $menu .= '</ul>';
//        Если категории нет выбиаем все
       if($id == null) {$products = Product::all();}
       else {
           $products = Product::join('product_categories', 'product_categories.product_id','=','products.id')
           ->where('product_categories.category_id',$id)
           ->select('products.id','products.title','products.description','products.price')
           ->get();
       }
        return view('front.index', compact('menu', 'products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.categories.create', ['categories' => Category::lists('title','id')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:2'
        ]);
        $input = $request->all();
        Category::create($input);
        Flash::message('Category '.$request->title.' Added!');
        return view('admin.categories.index', ['categories' => Category::all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('admin.categories.edit',
            ['category' => Category::find($id),
             'categories' => Category::lists('title','id')]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category = Category::findOrFail($id);
        $this->validate($request, [
            'title' => 'required|min:2',
        ]);
        $input = $request->all();
        $category->fill($input)->save();
        Flash::message('Category '.$request->title.' updated!');
        return redirect()->route('admin.categories.index');
    }
}

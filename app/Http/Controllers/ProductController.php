<?php

namespace App\Http\Controllers;

use App\Category;
use App\Image;
use App\Product;
use App\ProductCategory;
use Illuminate\Http\Request;
use App\Http\Requests;
use Laracasts\Flash\Flash;
use File;

class ProductController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.products.index', ['products' => Product::all()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.products.create', ['categories' => Category::lists('title','id')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required|min:2'
        ]);
        $input = $request->all();
        $id = Product::create($input);
        if(isset($input['cat_id'])) {
            $this->createCat($id, $input['cat_id']);
        }
        if(isset($input['image'])) {
            $this->createImage($id, $input['image']);
        }

            Flash::message('Product '.$request->title.' Added!');
        return view('admin.products.index', ['products' => Product::all()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        return view('admin.products.edit',
            ['product' => Product::find($id),
             'categories' => Category::lists('title','id'),
             'product_categories' => ProductCategory::where('product_id',$id)->lists('category_id'),
             'images' => Image::where('product_id',$id)->get(),
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $this->validate($request, [
            'title' => 'required|min:2',
        ]);
        $input = $request->all();
        ProductCategory::where('product_id',$id)->delete();

        if(isset($input['cat_id'])) {
            $this->cat($id, $input['cat_id']);
        }

        if(isset($input['image'])) {
            $this->createImage($id, $input['image']);
        }

        if(isset($input['del_image'])) {
            foreach($input['del_image'] as $item) {
                $img = Image::find($item);
                File::delete(public_path() . $img->scr);
                $img->delete();
            }
        }
        $product->fill($input)->save();

        return redirect()->route('admin.products.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function addCat(Request $request)
    {
        $input = $request->all();
        return view('_ajax.add_cat', ['categories' => Category::lists('title','id'), 'id' =>$input['id']+1]);
    }
    public function addImage(Request $request)
    {
        $input = $request->all();
        return view('_ajax.add_image', ['categories' => Category::lists('title','id'), 'id' =>$input['id']+1]);
    }

    public function createCat($id, $input) {
        $input = array_diff($input, array(0, null));
        $id->cat()->attach($input);
    }

    public function createImage($id, $input) {
        foreach($input as $item){
            $filename = str_random(15).'.'.$item
                    ->guessClientExtension();
            $item->move('images', $filename);
            $images_scr='/images/'.$filename;
            Image::create(['product_id'=>$id->id, 'scr'=>$images_scr]);
        }
    }
}

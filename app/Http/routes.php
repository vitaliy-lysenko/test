<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/
Route::group(['middleware' => 'web'], function() {
    Route::group(['prefix' => '/admin'], function(){
        Route::resource('/categories', 'CategoryController');
        Route::get('/products/add_cat', ['uses' => 'ProductController@addCat']);
        Route::get('/products/add_image', ['uses' => 'ProductController@addImage']);
        Route::get('/', ['uses' => 'ProductController@index']);
        Route::resource('/products', 'ProductController');
    });
    Route::get('/', ['uses' => 'IndexController@index']);
    Route::get('/{cat}', ['uses' => 'IndexController@index']);
    Route::get('/api/{id}', ['uses' => 'ApiController@getProductDetails']);
});
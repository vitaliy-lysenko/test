<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['scr', 'product_id'];

    public $timestamps = false;
}

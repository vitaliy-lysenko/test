<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['title', 'price', 'discount_count', 'discount_price', 'description'];

    public $timestamps = false;

    public function images() {
        return $this->hasMany('App\Image' , 'product_id');
    }
    public function cat() {
        return $this->belongsToMany('App\Category', 'product_categories');
    }
}

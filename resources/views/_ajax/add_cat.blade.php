@if(count($categories) > 0)
    <div class="form-group">
        {!! Form::label('cat_id', 'Parent category:', ['class' => 'control-label']) !!}
        {!! Form::select('cat_id[]', $categories, 0 , ['class' => 'form-control', 'placeholder' => 'none']) !!}
    </div>
@endif
<div class="form-group" id="add_cat{{$id}}" >
    <div class="btn btn-primary " onclick="add_cat('#add_cat{{$id}}', {{$id}})" >Add category</div>
</div>
<div class="form-group" >
    {!! Form::label('image[]', 'Image:', ['class' => 'control-label']) !!}
    {!! Form::file('image[]', ['class' => 'control-label'])  !!}
</div>
<div class="form-group" id="add_image{{$id}}" >
    <div class="btn btn-primary " onclick="add_image('#add_image{{$id}}', {{$id}})" >Add Image</div>
</div>
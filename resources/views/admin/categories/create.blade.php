@extends('admin')

@section('title')
Create categories
@endsection

@section('content')
    <div class="form-group">
        <h2>Create category</h2>
    </div>
    <div>
        <div class="form-group">
            @include('errors')
        </div>
        {!! Form::open(array('route' => 'admin.categories.store')) !!}
        <div class="form-group">
            {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
 @if(count($categories) > 0)
        <div class="form-group">
            {!! Form::label('parent_id', 'Parent category:', ['class' => 'control-label']) !!}
            {!! Form::select('parent_id', $categories, 0 , ['class' => 'form-control', 'placeholder' => 'none']) !!}
        </div>
@endif
        <div align="right">
            {!! Form::submit('Create', ['class' => 'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@extends('admin')

@section('title')
Edit categories
@endsection

@section('content')
    <div class="form-group">
        <h2>Edit category</h2>
    </div>
    <div>
        <div class="form-group">
            @include('errors')
        </div>
        {!! Form::model($category , ['method' => 'PATCH', 'route' => ['admin.categories.update', $category->id   ]]) !!}
        <div class="form-group">
            {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
 @if(count($categories) > 0)
        <div class="form-group">
            {!! Form::label('parent_id', 'Parent category:', ['class' => 'control-label']) !!}
            {!! Form::select('parent_id', $categories, null , ['class' => 'form-control', 'placeholder' => 'none']) !!}
        </div>
@endif
        <div align="right">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection
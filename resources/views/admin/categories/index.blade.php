@extends('admin')

@section('title')
Create categories
@endsection

@section('content')
   <div class="form-group">
   @include('message')
   </div>
   <div class="form-group">
      <a href="{{ route('admin.categories.create') }}">Add new category</a>
   </div>
   <div class="form-group">
      <ul>
         @if(count($categories) > 0)
            @foreach($categories as $category)
                <li><a href="{{ route('admin.categories.edit', $category->id) }}">{{$category->title}}</a></li>
            @endforeach
         @endif
      </ul>
   </div>
@endsection
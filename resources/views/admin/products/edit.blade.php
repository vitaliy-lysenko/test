@extends('admin')

@section('title')
Edit Product
@endsection

@section('content')
    <script>
        function add_cat(div_id, id){
            $.ajax({
                url: "/admin/products/add_cat",
                data: { div_id: div_id, id: id},
                success: function(txt){
                    $(div_id).html(txt);
                }});}

        function add_image(div_id, id){
            $.ajax({
                url: "/admin/products/add_image",
                data: { div_id: div_id, id: id},
                success: function(txt){
                    $(div_id).html(txt);
                }});}
    </script>
    <div class="form-group">
        <h2>Edit Product</h2>
    </div>
    <div>
        <div class="form-group">
            @include('errors')
        </div>
        {!! Form::model($product , ['method' => 'PATCH', 'route' => ['admin.products.update', $product->id   ],'files' => true]) !!}
        <div class="form-group">
            {!! Form::label('title', 'Title:', ['class' => 'control-label']) !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('price', 'Price:', ['class' => 'control-label']) !!}
            {!! Form::text('price', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('discount_price', 'Discount price:', ['class' => 'control-label']) !!}
            {!! Form::text('discount_price', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('discount_count', 'Count discount:', ['class' => 'control-label']) !!}
            {!! Form::text('discount_count', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('description', 'Description:', ['class' => 'control-label']) !!}
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>

        @if(count($categories) > 0)
          @foreach($product_categories as $product_category)
            <div class="form-group">
            {!! Form::label('cat_id', 'Parent category:', ['class' => 'control-label']) !!}
            {!! Form::select('cat_id[]', $categories, $product_category , ['class' => 'form-control', 'placeholder' => 'none']) !!}
             </div>
            @endforeach
        @endif
        <div class="form-group" id="add_cat1" >
            <div class="btn btn-primary " onclick="add_cat('#add_cat1', {{count($product_categories)}})" >Add category</div>
        </div>
        @foreach($images as $image)
            <div class="form-group">
                <img src="{{$image->scr}}" width="150px" height="130px">
                {!! Form::label('del_image', 'Delete:', ['class' => 'control-label']) !!}
                {!! Form::checkbox('del_image[]', $image->id, false) !!}
            </div>
        @endforeach
        <div class="form-group" id="add_image1" >
            <div class="btn btn-primary " onclick="add_image('#add_image1', 1)" >Add Image</div>
        </div>
        <div align="right">
            {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection
@extends('admin')

@section('title')
Create products
@endsection

@section('content')
   <div class="form-group">
   @include('message')
   </div>
   <div class="form-group">
      <a href="{{ route('admin.products.create') }}">Add new product</a>
   </div>
   <div class="form-group">
      <ul>
         @if(count($products) > 0)
            @foreach($products as $product)
                <li><a href="{{ route('admin.products.edit', $product->id) }}">{{$product->title}}</a></li>
            @endforeach
         @endif
      </ul>
   </div>
@endsection
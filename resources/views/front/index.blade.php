@extends('index')

@section('title')
 Index
@endsection

@section('menu')
    <ul>
       <li><a href="/">Index</a></li>
       <li><a href="/admin">admin</a></li>
    </ul>
    <br>
    <br>
    {!! $menu !!}
@endsection
@section('content')
    @if(count($products) > 0)
       @foreach($products as $product)
          <div class="form-group">
             <h2>{{ $product->title }}</h2>
             <div class="form-group">
                {{ $product->description }}
             </div>
             <div class="form-group">
                ${{ $product->price }}
             </div>
             <div class="form-group">
                @if(count($product->images) > 0)
                   @foreach($product->images as $image)
                      <img src="{{ $image->scr }}" alt="">
                   @endforeach
                @endif
             </div>
          </div>
       @endforeach
    @endif
@endsection